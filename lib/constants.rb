class Constants

  # There is other code in steps that need these to be identical
  # so best leave them alone. :)

  FILE_LOCATION = Rails.configuration.ink_api['file_location']
  INPUT_FILE_DIRECTORY_NAME = "input_files"
end